^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package espeak_ros
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.0 (2021-08-23)
------------------
* Better detect when the call to `espeak` fails
  If the audio device does not exist, espeak will just print a bunch of
  warnings to `stderr` but still return `0`. Now if any test is present
  in `stderr` we interpret it as a failure.
* Fix issues detected by linters
* Fix install directory of espeak_ros nodes
* Contributors: Thibaud Chupin
